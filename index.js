const equation = Math.pow (2, 3);
console.log(equation);

let message = `The cube of 2 is ${equation}`;
console.log(message);

const fullAddress = ["258", "Washington Ave", "NW", "California", "90011"];
const [houseNumber, avenue, direction, state, zipCode] = fullAddress;
console.log(`I live at ${houseNumber}, ${avenue}, ${direction}, ${state}, ${zipCode}.`);

const animal = {
	name: "Lolong",
	type: "Saltwater Crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in."
}

const {name, type, weight, measurement} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}.`);


class dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new dog();


myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";

console.log(myDog);